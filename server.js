var express = require('express');
var app = express();
var mongojs = require('mongojs');
var db = mongojs('shubham:project@ds147985.mlab.com:47985/meandb', ['menu']);
var bodyParser = require('body-parser');
var sync = require('synchronize')
var customername = "Shubham";
var event = "Event ";

app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());

function callback() {
    return;
}
app.use(function (req, res, next) {
    sync.fiber(next)
})

app.put('/save/:menu', function (req, res) {
        var menu = req.params.menu;
        db.menuIngredients.insert({customername: "Shubham", inventoryList: menu}, function (err, docsInserted) {
            _id = docsInserted._id;
            console.log(docsInserted._id);

        });
        res.json(docsInserted);
    }
);


app.put('/saveMenu/:menu', function (req, res) {
        var menu = req.params.menu;
        var ele = menu.split(',');
        // var query;
        console.log("Menu " + menu);

        //console.log("ELE " + ele);
        //console.log("ele length " + ele.length);
        var _id;
        for (var i = 0; i < ele.length; ++i) {
            console.log(i);
            if (i == 0) {
                console.log(i);

                db.menu.findOne({_id: mongojs.ObjectId(ele[i])}, function (err, docs) {
                    //console.log("------- Ele [" + i + "] : " + ele[i]);
                    console.log(docs);

                    db.menuIngredients.insert({customername: "Shubham", inventoryList: docs}, function (err, docsInserted) {
                        _id = docsInserted._id;
                        console.log(docsInserted._id);
                    });

                });

            }
            else if (i != 0) {
                db.menu.findOne({_id: mongojs.ObjectId(ele[i])}, function (err, docs) {
                    //setTimeout(callback,1000);

                    db.menuIngredients.findAndModify({
                        query: {_id: mongojs.ObjectId(_id)},
                        update: {$set: {inventoryList: docs}},
                        upsert: true
                    });

                    console.log(docs);
                });

            }
        }
    }
);

app.get('/menu', function (req, res) {
    console.log("I received a GET request Jain Menu")

    db.menu.find(function (err, docs) {
        console.log(docs);
        res.json(docs);
    });
});
app.get('/jainMenu', function (req, res) {
    console.log("I received a GET request Jain Menu")

    db.menu.find({"category": "Jain"}, function (err, docs) {
        console.log(docs);
        res.json(docs);
    });
});
app.get('/vegMenu', function (req, res) {
    console.log("I received a GET request Jain Menu")

    db.menu.find({"category": "Veg"}, function (err, docs) {
        console.log(docs);
        res.json(docs);
    });
});
app.get('/nonvegMenu', function (req, res) {
    console.log("I received a GET request Jain Menu")

    db.menu.find({"category": "NonVeg"}, function (err, docs) {
        console.log(docs);
        res.json(docs);
    });
});

app.get('/retrieveDishDetails/:id', function (req, res) {
    var id = req.params.id;
    console.log(id);
    db.menu.findOne({_id: mongojs.ObjectId(id)}, function (err, docs) {
        //query = docs.toString();
        console.log(docs);
        res.json(docs);

    });

});


/*
 db.menuIngredients.insert(req, function (err, doc) {
 res.json(doc);
 })
 //console.log("Add to menu id " + id);

 /*db.contactlist.insert(req.body,function (err, doc) {
 res.json(doc);
 })
 */


app.delete('/contactlist/:id', function (req, res) {
    var id = req.params.id;
    console.log(id);
    db.contactlist.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
        res.json(doc);
    })
});

app.get('/contactlist/:id', function (req, res) {
    var id = req.params.id;
    console.log(id);
    db.contactlist.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
        res.json(doc);
    });
});

app.put('/contactlist/:id', function (req, res) {
    var id = req.params.id;
    console.log(req.body.name);
    db.contactlist.findAndModify({
        query: {_id: mongojs.ObjectId(id)},
        update: {$set: {name: req.body.name, email: req.body.email, number: req.body.number}},
        new: true
    }, function (err, doc) {
        res.json(doc);
    });
});

app.listen(3000);
console.log("server running on port 3000");
