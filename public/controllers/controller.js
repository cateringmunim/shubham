var myApp = angular.module('myApp', []);
myApp.controller('AppCtrl', ['$scope', '$http',
    function ($scope, $http) {
        console.log("Hello World from controller");
        var menuitems = [];

        var refresh = function () {
            $http.get('/menu').success(function (response) {
                console.log("I got the data");
                $scope.menuitems = response;
                $scope.menu = "";
            });
        };

        refresh();
        $scope.jainMenu = function () {
            console.log($scope.menu);
            $http.get('/jainMenu').success(function (response) {
                console.log("I got the Jain Menu data");
                $scope.menuitems = response;
                $scope.menu = "";
            });
        };
        $scope.vegMenu = function () {
            console.log($scope.menu);
            $http.get('/vegMenu').success(function (response) {
                console.log("I got the Veg Menu data");
                $scope.menuitems = response;
                $scope.menu = "";
            });
        };
        $scope.nonvegMenu = function () {
            console.log($scope.menu);
            $http.get('/nonvegMenu').success(function (response) {
                console.log("I got the Veg Menu data");
                $scope.menuitems = response;
                $scope.menu = "";
            });
        };
        $scope.addToMenu = function () {
            var tags = document.getElementsByName('tags');
            console.log(tags.length);
            for (var ind = 0; ind < tags.length; ++ind) { // console.log('\n' + ind);
                if (tags[ind].checked) {
                    // console.log('\t' + ind + 'checked');

                    flag = false;
                    for (var i = 0; i < menuitems.length; i++) {
                        if (tags[ind].value == menuitems[i])
                            flag = true;
                    }
                    if (!flag) {
                        menuitems.push(tags[ind].value);
                        console.log('added   :' + menuitems);
                    }
                }
                if (!tags[ind].checked) {
                    //console.log('\t' + ind + 'unchecked');
                    for (var i = 0; i < menuitems.length; i++) {
                        //console.log('\t' + ind + tags[ind].value);
                        if (tags[ind].value == menuitems[i]) {
                            menuitems.splice(i, 1);
                            console.log('removed   :' + menuitems);
                        }

                    }

                }
                document.getElementById('menuitm').innerHTML = menuitems;
                document.getElementById('menuitmlen').innerHTML = menuitems.length;
            }
        }

        var dishes = [];
        var retrieve = function (id) {
            $http.get('/retrieveDishDetails/' + id).success(function (response) {
                console.log(response);
                dishes.push(response);


            });

        }
        $scope.saveMenu = function () {
            console.log("saveMenu()");
            console.log("Menuitems" + menuitems);
            /*for (i = 0; i < menuitems.length; ++i) {
             retrieve(menuitems[i]);
             console.log("Dishes : " + dishes);
             }
             */
            $http.put('/saveMenu/' + menuitems).success(function (response) {
                console.log(response);

                refresh();
            });
            /*
             $http.put('/save/' + dishes).success(function (response) {
             console.log(response);

             refresh();
             });
             */
        };


        /*
         $scope.addContact = function () {
         console.log($scope.contact);
         $http.post('/contactlist', $scope.contact).success(function (response) {
         console.log(response);
         refresh();
         });
         };


         $scope.remove = function (id) {
         console.log(id);
         $http.delete('/contactlist/' + id).success(function (response) {
         refresh();
         });
         };

         $scope.edit = function (id) {
         console.log(id);
         $http.get('/contactlist/' + id).success(function (response) {
         $scope.contact = response;
         });
         };

         $scope.update = function () {
         console.log($scope.contact._id);
         $http.put('/contactlist/' + $scope.contact._id, $scope.contact).success(function (response) {
         refresh();
         });
         };

         $scope.deselect = function () {
         $scope.contact = "";
         }
         */
    }

]);
